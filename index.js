import React from 'react';
import { render } from 'react-dom';
import Hello from './src/containers/Hello';

render(
    <Hello />,
  document.getElementById('root')
);
